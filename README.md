# srtssa

This program takes an .srt, .txt, .epub, or .pdf file, translates it, and merges both translations into
an .ssa or .txt file in a parallel manner, allowing both subtitles to be viewed at the same time.
This can be helpful for people learning a new language.


Currently, my tests indicate that it can translate English into 102 languages:


1. Afrikaans Afrikaans [af]
1. Albanian Shqip [sq]
1. Amharic አማርኛ [am]
1. Arabic العربية [ar]
1. Armenian Հայերեն [hy]
1. Azerbaijani Azərbaycanca [az]
1. Basque Euskara [eu]
1. Belarusian беларуская [be]
1. Bengali বাংলা [bn]
1. Bosnian Bosanski [bs]
1. Bulgarian български [bg]
1. Catalan Català [ca]
1. Cebuano Cebuano [ceb]
1. Chichewa Nyanja [ny]
1. Chinese Simplified 简体中文 [zh-CN]
1. Chinese Traditional 正體中文 [zh-TW]
1. Corsican Corsu [co]
1. Croatian Hrvatski [hr]
1. Czech Čeština [cs]
1. Danish Dansk [da]
1. Dutch Nederlands [nl]
1. Esperanto Esperanto [eo]
1. Estonian Eesti [et]
1. Filipino Tagalog [tl]
1. Finnish Suomi [fi]
1. French Français [fr]
1. Frisian Frysk [fy]
1. Galician Galego [gl]
1. Georgian ქართული [ka]
1. German Deutsch [de]
1. Greek Ελληνικά [el]
1. Gujarati ગુજરાતી [gu]
1. Haitian Creole Kreyòl Ayisyen [ht]
1. Hausa Hausa [ha]
1. Hawaiian ʻŌlelo Hawaiʻi [haw]
1. Hebrew עִבְרִית [he]
1. Hmong Hmoob [hmn]
1. Hungarian Magyar [hu]
1. Icelandic Íslenska [is]
1. Igbo Igbo [ig]
1. Indonesian Bahasa Indonesia [id]
1. Irish Gaeilge [ga]
1. Italian Italiano [it]
1. Japanese 日本語 [ja]
1. Javanese Basa Jawa [jv]
1. Kannada ಕನ್ನಡ [kn]
1. Kazakh Қазақ тілі [kk]
1. Kinyarwanda Kinyarwanda [rw]
1. Korean 한국어 [ko]
1. Kurdish Kurdî [ku]
1. Kyrgyz Кыргызча [ky]
1. Lao ລາວ [lo]
1. Latin Latina [la]
1. Latvian Latviešu [lv]
1. Lithuanian Lietuvių [lt]
1. Luxembourgish Lëtzebuergesch [lb]
1. Macedonian Македонски [mk]
1. Malagasy Malagasy [mg]
1. Malay Bahasa Melayu [ms]
1. Malayalam മലയാളം [ml]
1. Maltese Malti [mt]
1. Maori Māori [mi]
1. Mongolian Монгол [mn]
1. Norwegian Norsk [no]
1. Pashto پښتو [ps]
1. Persian فارسی [fa]
1. Polish Polski [pl]
1. Portuguese Português [pt]
1. Romanian Română [ro]
1. Russian Русский [ru]
1. Samoan Gagana Sāmoa [sm]
1. Scots Gaelic Gàidhlig [gd]
1. Serbian (Cyrillic) српски [sr-Cyrl]
1. Serbian (Latin) srpski [sr-Latn]
1. Sesotho Sesotho [st]
1. Shona chiShona [sn]
1. Sindhi سنڌي [sd]
1. Sinhala සිංහල [si]
1. Slovak Slovenčina [sk]
1. Slovenian Slovenščina [sl]
1. Somali Soomaali [so]
1. Spanish Español [es]
1. Sundanese Basa Sunda [su]
1. Swahili Kiswahili [sw]
1. Swedish Svenska [sv]
1. Tajik Тоҷикӣ [tg]
1. Tamil தமிழ் [ta]
1. Tatar татарча [tt]
1. Telugu తెలుగు [te]
1. Thai ไทย [th]
1. Turkish Türkçe [tr]
1. Turkmen تۆرکمنچه‎ [tk]
1. Ukrainian Українська [uk]
1. Urdu اُردُو [ur]
1. Uyghur ئۇيغۇر تىلى [ug]
1. Uzbek Oʻzbek tili [uz]
1. Vietnamese Tiếng Việt [vi]
1. Welsh Cymraeg [cy]
1. Xhosa isiXhosa [xh]
1. Yiddish ייִדיש [yi]
1. Yoruba Yorùbá [yo]
1. Zulu isiZulu [zu]еларуская [be]


The program will translate the following languges but very slowly (line by line):

* Hindi हिन्दी [hi]
* Khmer ភាសាខ្មែរ [km]
* Marathi मराठी [mr]
* Myanmar (Burmese) မြန်မာစာ [my]
* Nepali नेपाली [ne]
* Odia (Oriya) ଓଡ଼ିଆ [or]
* Punjabi ਪੰਜਾਬੀ [pa
]


As for translating from one of the above languages into another, there are just too many combinations for me to test. The program has been heavily tested with English, French, and Russian, and seems to do fine translating from or to other languages, but it is for you to test your combination.

See executable file for usage information.

*** NOTE ***

This code was developed on Fedora 32. Having just ran the code on Debian 10, I found several bugs which are likely related outdated versions of programs:

1. *FIXED*
2. Debian 10's version of Perl does not support 'lookbehind', which is needed by the script's default translation engine. A workaround is to use the flag '--alt' to allow translate-shell to be used instead.
3. If using translate-shell, make sure to use an updated version (on Debian, you must make and install the program yourself as Debian's version is out of date and has bugs) or you may encounter buggy characters: https://github.com/soimort/translate-shell
 
When I get time, I'll look into fixing these issues.
