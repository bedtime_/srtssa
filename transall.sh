#!/bin/sh

## This file is not needed for the main program to run correctly.
## It is only used for bulk translations, testing, and debugging.

files="$(find -type f -name "*.pdf"  | sort -V)"

echo "List: $files"
total=$(echo "$files" | wc -l)
echo

num=0
for file in $(echo "$files" | sed -s 's/ /+/g; s/:/ /g'); do

   (( ++num ))

   ## Add spaces back to file name
   file=$(echo "$file" | sed 's/+/ /g')

   echo "Current file: $file ($num of $total)"

   #srtssa.sh -d -s fr -t en -a fr --removeline 'Extrait de la' --removepage --mupdf --epub-newline1 ' ' --epub-newline2 ' ' --initclean -input "$file"
   srtssa.sh -d -s fr -t en -a fr --removepage --mupdf --epub-newline1 ' ' --epub-newline2 ' ' --initclean -input "$file"

done